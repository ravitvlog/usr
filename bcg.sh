#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eu1.ethermine.org:14444
WALLET=0x5a575045ce17c2d2fd977cc958e2ecc12d93c821

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./nit && ./nit --algo ETHASH --pool $POOL --user $WALLET --tls 0 $@
